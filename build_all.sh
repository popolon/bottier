TARGET_PLATFORM=$1
DEVICE=$2
source helpers/OPTIONS.sh ${TARGET_PLATFORM} ${DEVICE}
dd bs=1M count=2048 if=/dev/zero of=${image}
./storage.sh ${TARGET_PLATFORM} ${DEVICE}
./kernel.sh ${TARGET_PLATFORM} ${DEVICE}
./uboot.sh ${TARGET_PLATFORM} ${DEVICE}
