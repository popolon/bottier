Bottier \bɔ.tje\ is a bootmaker workshop. Its goal is to automate and centralize common tasks, for making various architectures bootables filesystems.

Will try to avoid BIOS/UEFI/ACPI bloatware garbage as much as possible, in favor of FLOSS and well designed softwares and drivers, using `Device Tree <https://www.devicetree.org/>`_ ecosystem instead.

It's FLOSS under `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.html>`_ + `MulanPSL-2.0 <https://opensource.org/license/mulanpsl-2-0/>`_ licenses.


**Please don't clone this repository on Github, if you upload a copy on Github, you would violate the two FLOSS licenses**. This is due to `copilot license violation <https://www.theregister.com/2022/11/11/githubs_copilot_opinion/>`_. Uploading it on Github would then be a copyright violation. You can connect to this instance from your Github account to this instance if you want to interact.

**This software is given without any warranty, it can burn your computer, your city or the whole planet, depending on how you use it**

To install it, without reading the goal, features etc, go to Requirements and then Usage sections.


Get the sources, report the bugs, improve it
============================================
The main repo is `https://framagit.org/popolon/bottier <https://framagit.org/popolon/bottier>`_

`Framagit <https://framagit.org>`_ follows `chatons <https://www.chatons.org/en>`_ rules.


Features and goals
==================
* Compile natively or use cross-compilation if needed.
* Use sources or gather precompiled binaries open-sources binaries for low-power resources computers or just gain time.
* Build u-boot + OpenSBI (for RISC-V ISA)
* Build u-boot + ARM Trusted Firmware (`for Rockchip <https://opensource.rock-chips.com/wiki_Boot_option>`_ as a start) (TODO)
* For the future, manage `LibreBoot <https://libreboot.org/>`_/`CoreBoot <https://www.coreboot.org/>`_/`oreBoot <https://github.com/oreboot>`_/… (TODO)
* `LinuxBoot <https://www.linuxboot.org/>`_ if possible (TODO)
* Build kernel from any source, or mainline, with specified patchs, first made to build linux-mainline-next and some minor patchs (work on d1, but not generalized very experimental)
* Get sources from git or tarballs
* Autorecognize .gz/bz2/zstd/xz for patchs
* Build the target storage structure
* Build and install on raw file, using loopback, allowing testing on emulators (as example) first and deploy after validation
* Install rootfs for Arch Linux
* Install rootfs for Debian (Debootstrap or cdebootstrap) (TODO)
* Install or update independently u-boot, kernel and OS (DONE But not OS installation)

As first targets
================

Both as start, hope it would grow.

Hardware targets
----------------
* Allwinner D1 (RISC-V 64 CPU ISA (C906)) (Done). Note `Realtek RTL8723DS Wireless chip has been merged in linux 6.5, previous version need a patch (as shown in recipies <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=3a8a670eeeaa40d87bd38a587438952741980c18>`_)
* Rockchip RK3588 (ARM64 CPU ISA) (WIP, boot part was especially complex, due to some still closed firmwares, should be easier, end 2024, due to the `work of Collabora <https://gitlab.collabora.com/hardware-enablement/rockchip-3588/>`_. See their blog about opening `RK3588 <https://www.collabora.com/news-and-blog/blog/2024/02/21/almost-a-fully-open-source-boot-chain-for-rockchips-rk3588/>`_ and `RK3576 <https://www.collabora.com/news-and-blog/news-and-events/initial-upstream-support-for-the-rockchip-rk3576.html>`_)
* Bouffalo BL808 (2*RISC-V 32 MCU (E907 + E902) + 64 CPU (C906) ISA) (maybe ?)
* T-Head TH1520 (4* RISC-V 64 CPU (C910) + RISC-V 64 CPU (C906) for DSP + RISC-V 32 MCU (E902) for AON system + GPU) (WIP)

Allwinner D1 has several version:

* Mainline, without HDMI output (RVV 0.7.1 is mainlined under the name of THead)
* Smaeul line including HDMI output, with vector patch by Robin Lu, need riscv64-unknown-linux-gnu-gcc (GCC) 10.2.0 from THead patched toolchain
* Smaeul line including HDMI output, without vector patch and HDMI, can be compiled with last GCC (currently 14.2.1)

Ideally HDMI patch should be mainlined or adapted to mainline at least.

OS targets
----------
* `Arch Linux <https://archlinux.org>`_ (ARM and RISC-V flavors)
* `Debian <https://debian.org>`_ (ARM and RISC-V flavors)
* `OpenSUSE <https://opensuse.org>`_ (ARM and RISC-V flavors) (**WISH state only**)
* `Haiku <https://www.haiku-os.org/>`_ (RISC-V flavor) (**WISH state only**)
* Maybe `R9 (Rust version of Plan9) <https://github.com/r9os/r9/>`_ (RISC-V flavor) (**WISH state only**)
* The goal is to be easily user extendible using mainly configuration files.

Requirements
============
if your OS doesn't have cross-compilers, you can use native compiler with qemu+native rootfs.


Arch Linux and derivatives
--------------------------
``pacman -S base-devel dtc``

Depending on your target(s) and current architecture(s)

**RISC-V RV64 from other architecture**

``pacman -S riscv64-linux-gnu-gcc riscv64-slinux-gnu-binutils``

**ARM AARCH64 from other architecture**

``pacman -S aarch64-linux-gnu-gcc aarch64-linux-gnu-binutils``

Qemu can help for testing on compiling if cross-compile gcc is unavailable:

``pacman -S qemu-system-aarch64 qemu-system-riscv qemu-user-binfmt``

In this case, You should take a rootfs, I would suggest Arch Linux, that always contains last stable versions of compilers and tools and so have more chance to manage your specific architecture.

* `Last Arch Linux for RISC-V generic rootfs <https://archriscv.felixc.at/images/archriscv-2023-06-07.tar.zst>`_
* `Last Arch Linux for ARM64 generic rootfs <http://de3.mirror.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz>`_


Debian and derivatives
----------------------
``apt install build-essential device-tree-compiler``

Depending on your target(s) architecture(s)

**RISC-V RV64 from other architecture**

``apt install gcc-riscv64-linux-gnu binutils-riscv64-linux-gnu``

**ARM AARCH64 from other architecture**

``apt install gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu``

Qemu can help for testing or compiling if cross-compile gcc is unavailable (risc-v is in misc)

``apt install qemu-system-misc qemu-system-arm qemu-user-binfmt``

In this case, you can use `debootstrap scripts <https://wiki.debian.org/Debootstrap>`_ or smaller C version, `cdebootstrap <https://packages.debian.org/search?keywords=cdebootstrap>`_


OpenSUSE and derivatives
------------------------
(TODO)

Usage
=====
1. Set your preferences in conf/bottier.conf
2. Use one of the hardware target configuration or create a new one. when you launch one of the programs without arguments, it will give you the availables receipes.
3. Fetch and compile u-boot and patchs with `./u-boot.sh target` (where target is, for exemple `d1`)
4. Fetch and compile kernel and patchs (and external modules) with `./kernel.sh target`
5. (optional): to fetch+compile+install (will not refetch/compile if already done) `./kernel.sh target /dev/mmcblk0` (or other device) **Use with care**
6. Prepare disk image paritioning (basis done)
7. Install uboot (Work on risc-v, TOFINISH)

Produce a disk image
====================

A methode to create a 2GB blank file:

``dd bs=1M count=2048 if=/dev/zero of=image.raw``

Then to prepare partitions on the image to receive the uboot/kenel/modules/OS:

``./storage.sh <receipe> image.raw``

Then  kernel can be installed the following way:

``./kernel.sh <receipe> image.raw``

And Das U-boot (need to be more tested on RISC-V, WIP on RK3588):

``./uboot.sh <receipe> image.raw``


Configuration files
-------------------
There are some exemples files, this is a first of the software, they can change at any moment (and they changed several times due to changes in kernel sources, and there tarball sources, and they will, probably until v1.0.

Useful references
==================

* `Das U-Boot documentation <https://u-boot.readthedocs.io/en/latest/>`_
* `LinuxBoot <https://www.linuxboot.org/>`_
* `Linux kernel documentation <https://docs.kernel.org/>`_
* `Device Tree on Linux kernel documantation <https://docs.kernel.org/devicetree/index.html>`_

D1
--
* `Allwinner D1 official documentation <https://d1.docs.aw-ol.com/en/>`_
* `Linux Sunxi Community Wiki (for AllWinner SoCs <https://linux-sunxi.org/>`_
* `Sipeed RV schematic <https://popolon.org/depots/RISC-V/D1/LicheeRV/dl.sipeed.com/HDK/2_Schematic/Lichee_RV_7001_Schematic.pdf>`_  LCD Socket: SCK=PC2/LCD_SCK MOSI=PC4 RS=PC5, CS=PC3, RESET=PC6 LEDA(screen backlight)=PD18
* `panel SItronix ST7789v driver DT description <https://www.kernel.org/doc/Documentation/devicetree/bindings/display/panel/sitronix,st7789v.yaml>`_
* `Tina Linux System (from AllWinner) <https://d1.docs.aw-ol.com/en/study/study_1tina/>`_
* `LicheeRV BSP Rebuild (on GitHub) <https://github.com/Jookia/LicheeRVBSPRebuil>`_
* `Linaro <https://www.linaro.org/>`_
* `for the MiPi compatible screen <https://www.kernel.org/doc/Documentation/devicetree/bindings/display/panel/panel-mipi-dbi-spi.yaml>`_

RK3588
-------
* `Rockchip Linux (on GitHub) <https://github.com/rockchip-linux/>`_
* `Radxa wiki on Rock 5b <https://wiki.radxa.com/Rock5/5b>`_ and `Rock 5 ITX <https://docs.radxa.com/en/rock5/rock5itx>`_
* `Rock-5b on ARMbian wiki <https://www.armbian.com/rock-5b/>`_  `Rock 5 ITX <https://www.armbian.com/radxa-rock-5-itx/>`_
* `Rock-5 in general with BredOS <https://wiki.bredos.org/en/rock-5>`_ Arch derived disribution specialized on RK3588
* `BredOS-ARM-Rock5-ITX-2024-12-04.img.xz <https://github.com/BredOS/images/releases/download/2024-12-04/BredOS-ARM-Rock5-ITX-2024-12-04.img.xz>`_
BL808
-----
* `OpenBouffalo Firmware <https://github.com/openbouffalo/OBLFR>`_
* `OpenBouffalo SDK <https://github.com/bouffalolab/bouffalo_sdk>`_
* `T-Head Linux Tool Chain <https://gitee.com/bouffalolab/linuxtoolchain_gcc_t-head>`_

Boards:
* `Sipeed M1S Dock <https://wiki.sipeed.com/hardware/en/maix/m1s/m1s_dock.html>`_
* `Pine64 Ox64 <https://wiki.pine64.org/wiki/Ox64>`_


TH1520
------
* `U-boot based on T-Head version <https://github.com/revyos/thead-u-boot>`_ until it is mainlined.
* Kernel based on `mainstream Linux-next <https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/tree/arch/riscv/boot/dts/thead?h=next-20230627&id=5af4cb0c42c5ee084c40ae37f6ecce839b4f65bc>`_, reached in time for Linux 6.5 next release.
* Tests on `Sipeed Lichee Pi4 board <https://sipeed.com/licheepi4a/>`_ `documentation (WIP) <https://wiki.sipeed.com/hardware/en/lichee/th1520/lpi4a/1_intro.html>`_
* There are premade image from Sipeed and their partners with DeepinOS and OpenKylin here : https://github.com/aiminickwong/licheepi4a-images 
* GLES support (as non mainlined in jully 2023) https://github.com/revyos/thead-gles-addons/tree/master/addons + https://mirror.iscas.ac.cn/revyos/revyos-gles-21/pool/main/m/mesa/ + https://mirror.iscas.ac.cn/revyos/revyos-gles-21/pool/main/libd/libdrm/ + https://mirror.iscas.ac.cn/revyos/revyos-gles-21/pool/main/x/xorg-server/ or https://mirror.iscas.ac.cn/revyos/revyos-gles-21/  patch for GLES on debian: https://github.com/revyos/thead-gles-addons/tree/master/addons/usr/lib
