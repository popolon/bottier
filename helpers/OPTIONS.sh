TARGET_PLATFORM=$1
DEVICE=$2
if [ "$TARGET_PLATFORM" == "" -o ! -e "conf/${TARGET_PLATFORM}.conf" ]
then
  echo "Syntax: $0 TARGET_PLATFORM [/dev/device_to_write_on]"
  echo -e "\e[1mTARGET_PLATFORM must be an available  configuration file in conf/TARGET_PLATFORM.conf\e[0m"
  echo "list of available conf:"
  ls conf/ | grep conf$ | grep -v bottier.conf | grep -v template | cut -d . -f 1| while read CONF
  do
    echo "${CONF}:`grep ^#description: conf/${CONF}.conf | cut -d : -f 2`"
  done
  exit -1
fi
source "conf/${TARGET_PLATFORM}.conf"
source "conf/bottier.conf"
echo "DEVICE=${DEVICE}"
echo THREADS=${THREADS}
BASE_DIR=`pwd`

UBOOT_DIR=`basename ${UBOOT_SOURCE}`
OS_FILE=`basename ${OS_URL}`

R='\033[1;31m'
G='\033[1;32m'
Y='\033[1;33m'
B='\033[1;34m'
C='\033[1;36m'
it='\O33[3m'
ul='\033[4m'
bl='\033[5m'
in='\033[7m'
ov='\033[9m'
plain='\033[0m'


if [ "${DEVICE}" != "" ]
then
  if [ ${DEVICE:0:5} = "/dev/" ]
  then
    if [ "${DEVICE:5:2}"  == "sd" ]
    then
      p=number
    else
      p=pnumber
    fi
  else
    image_file=${DEVICE}
    DEVICE=`losetup -fP --show ${image_file}`
    echo "mount ${image_file} as ${DEVICE}"
    p=pnumber
  fi
  if [ "$p" == "pnumber" ]
  then
    BOOTPART_DEV=${DEVICE}p0
    ROOTPART_DEV=${device}p1
  else
    BOOTPART_DEV=${DEVICE}0
    ROOTPART_DEV=${DEVICE}1
  fi
fi

if [ "${THREADS}" != "" ]
then
 THREADS_OPT=-j${THREADS}
 echo THREADS_OPT=${THREADS_OPT}
else
 THREADS_OPT=
 CPUI=`awk '/^processor/{print $3}' </proc/cpuinfo | tail -n 1`
 THREADS=`expr $CPUI + 1`
fi

HOST_ARCH=`uname -m`
echo "HOST_ARCH=$HOST_ARCH ARCH=$ARCH IARCH=$IARCH"

if [ "$HOST_ARCH" != "${ARCH}" ]
then
 CROSS_COMPILER_OPT="CROSS_COMPILE=${CROSS_COMPILE} ARCH=${ARCH}"
fi

COMPILER_OPT="${THREADS_OPT} ${CROSS_COMPILER_OPT}"

echo "COMPILER_OPT=${COMPILER_OPT}"

BUILDER_DIR="builder/${TARGET_PLATFORM}"

if [ ! -e "${BUIDLER_DIR}" ]
then
  mkdir -p "${BUILDER_DIR}"
  if [ $? -ne 0 ]
  then
    echo "Can't create builder dir: ${BUILDER_DIR}, exit"
    exit -3
  fi
fi

if [ "${KERNEL_DOWN}" == "git" ]
then
 KERNEL_DIR=${KERNEL_URL##*/}
else
 KERNEL_DIR=linux-${KERNEL_BASE}
fi

#####
#exit 0
#####

goto_target_dir()
{
  cd "${BUILDER_DIR}"
}

goto_base_dir()
{
  cd "${BASE_DIR}"
}

are_you_really_sure()
{
alert="${1}"
  echo ${alert}
  echo "type in full letter: YES I WANT"
  read answer
  if [ "${answer}" == "YES I WANT" ]
  then
    return 0
  else
    exit -127
  fi
}

bottier_fetch()
{
URL="$1"
ACTION="$2"
OPTION="$3"
FILE="${URL##*/}"
  if [ "${URL}" == "" ]
  then
    echo "fetch: URL=${URL}, need a valide URL"
    exit -2
  fi
  if [ "${ACTION}" == "git" ]
  then
    if [ -e "${URL##*/}" ]
    then
      cd "${URL##*/}"
      git pull
      cd ..
    else
      if [ "${OPTION}" != "" ]
      then
        echo "GIT clone ${URL} -b ${OPTION}"
        git clone --depth 1 "${URL}" -b ${OPTION}
      else
        echo "GIT clone ${URL}"
        git clone --depth 1 "${URL}"
      fi
    fi
  else
    if [ ! -e "${FILE}" ]
    then
      wget "${URL}"
    fi
    if [ "${ACTION}" == "untar" ]
    then
      tar xf "${FILE}"
    fi
  fi
}

filecat()
{
SRC="${1}"
EXT="${SRC//*.}"
#  if [ "${FILE:0:1}" != "/" ]
#  then # not absolute path
#    SRC=../${SRC}
#  fi
  case "${EXT}" in
    "gz")
      zcat "${SRC}"
      ;;
    "bz2")
      bzcat "${SRC}"
      ;;
    "xz")
      xzcat "${SRC}"
      ;;
    "zst")
      zstdcat "${SRC}"
      ;;
    *)
     cat "${SRC}"
  esac
}

apply_patch()
{
DIR="${1}"
PATCH="${2}"
EXT="${PATCH//*.}"
  cd "${DIR}"
  if [ "${FILE:0:1}" != "/" ]
  then # not absolute path
    PATCH="${BASE_DIR}/${PATCH}"
  fi 
  echo "apply patch: ${PATCH} in dir ${DIR}"
  PATCH_NAME=`basename ${PATCH//.${EXT}}`
  if [ -e "${PATCH_NAME}" ]
  then
    echo "${PATCH_NAME} already applyed doesn't apply it again"
    return 0
  else
    echo "applied" >"${PATCH_NAME}"
  fi
  if [ "${EXT}" == "gz" ]
  then
    zcat "${PATCH}" | patch -p1
  elif [ "${EXT}" == "bz2" ]
  then
    bzcat "${PATCH}" | patch -p1
  elif [ "${PATCH}" == "xz" ]
  then
    xzcat "${PATCH}" | patch -p1
  elif [ "${PATCH}" == "zst" ]
  then
    zstdcat "${PATCH}" | patch -p1
  else
    patch -p1 <"${PATCH}"
  fi
  cd -
}

table_part()
{
TABLE=$1 #STORAGE_PARTS
LINE=$2 #STORAGE_LINE
  i=0
  for str in ${TABLE[${LINE}]}
  do
    partitions[${LINE},$i]="$str"
    ((i++))
  done
}

unloop()
{
  if [ "${DEVICE:5:4}" == "loop" ]
  then
    echo "../undo loop device ${DEVICE}"
    losetup -d ${DEVICE}
  fi
}

mount_fs()
{
# mount and install rootfs
  if [ "${DEVICE}" != "" ]
  then
    if [ ! -e "${MNT}" ]
    then
      echo "${MNT} doesn't exists, create it"
      sudo mkdir -p "${MNT}"
      rc=$?
      if [ $rc != 0 ]
      then
        echo "can't create ${MNT}, exit"
        exit -1
      fi
    fi
    echo "..mount ${DEVICE}p2 \"${MNT}\""
    sudo mount ${DEVICE}p2 "${MNT}"
    if [ ! -e "${MNT}/boot" ]
    then
      echo "${MNT}/boot doesn't exists, create it"
      sudo mkdir -p "${MNT}/boot"
    fi
    echo "..mount ${DEVICE}p1 \"${MNT}/boot/\""
    sudo mount ${DEVICE}p1 "${MNT}/boot/"
  fi
}

umount_fs()
{
  if [ "${DEVICE}" != "" ]
  then
    sudo umount "${MNT}/boot/"
    sudo umount "${MNT}"
    sync
    unloop
  fi
}
