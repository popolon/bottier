#!/usr/bin/env bash
TARGET_PLATFORM=$1
DEVICE=$2
source helpers/OPTIONS.sh ${TARGET_PLATFORM} ${DEVICE}

opensbi_fetch()
{
  if [ "${OPENSBI_FROM}" == "bin" ]
  then
    echo "OpenSBI from bin"
    bottier_fetch "${OPENSBI_BIN_SRC}" untar
    OPENSBI_BIN=${OPENSBI_BIN_BIN}
  elif [ "${OPENSBI_FROM}" == "src" ]
  then
    echo "OpenSBI from src"
    bottier_fetch "${OPENSBI_SRC_SRC}" untar
    OPENSBI_BIN=${OPENSBI_SRC_BIN}
  else
   echo "OPENSBI_FROM not defined, please define it"
   exit -1
  fi
}

opensbi_compile()
{
  echo "#####################"
  echo "compile OpenSBI"
  echo "#####################"
  cd "opensbi-${OPENSBI_VER}"
  make ${COMPILER_OPT} PLATFORM=generic FW_PIC=y FW_OPTIONS=0x2
  cd ..
}

rkboot_fetch()
{
  bottier_fetch "${RKBIN_SOURCE}" git
  RKBIN_DIR=`basename ${RKBIN_SOURCE}`
}

#rkboot_compile()
#{
#  uboot_build
#  IDBLOCK_BUILDTOOL="mkimage" ./uboot.sh -d rock-5b
#}

uboot_fetch()
{
  if [ ! -e "${UBOOT_DIR}" ]
  then
    if [ "${UBOOT_DOWN}" == "git" -o "${UBOOT_DOWN}" ==  "" ]
    then
      bottier_fetch "${UBOOT_SOURCE}" git ${UBOOT_BRANCH}
    else
      bottier_fetch "${UBOOT_URL}" untar
    fi
  else
    cd ${UBOOT_DIR}
    git checkout "${UBOOT_BRANCH}"
    git pull
    cd ..
  fi
}

## U-Boot configuration
uboot_config()
{
  echo "u-boot config"
  pwd
  cd ${UBOOT_DIR}
  if [ "${UBOOT_CONFIG_FROM}" != "" ]
  then
    echo "UBOOT_CONFIG_FROM: ${UBOOT_CONFIG_FROM}"
    case "${UBOOT_CONFIG_FROM}" in
      'defconfig')
        echo "..from defconfig: configs/${UBOOT_CONFIG}"
        echo cp -a configs/${UBOOT_CONFIG} .config
        cp -a configs/${UBOOT_CONFIG} .config
        ;;
      'file')
        if [ "${UBOOT_CONFIG:0:1}" != "/" ]
        then # not absolute path
          echo "..use ${BASE_DIR}/${UBOOT_CONFIG} oldconfig"
          filecat ${BASE_DIR}/${UBOOT_CONFIG} >.config
        else
          echo "..use  ${UBOOT_CONFIG} oldconfig"
          filecat ${UBOOT_CONFIG} >.config
        fi
        make ${COMPILER_OPT} oldconfig
        ;;
      'menuconfig')
        make ${COMPILER_OPT} menuconfig
        ;;
      'defconfig')
	make ${COMPILER_OPT} defconfig
	;;
#      *)
#        make ${COMPILER_OPT} menuconfig
#        ;;
    esac
  fi
  ####
  #### to help improve an existing TODO: if UBOOT_CONFIG_FROM == "file/kerneldir+menuconfig"
  #make ${COMPILER_OPT} menuconfig
  ###
  # exit 0
  ###
  cd ..
}

uboot_compile()
{
  if [ "${OPENSBI_FROM}" == "src" ]
  then
    opensbi_compile
  fi
  echo "########################"
  echo "Compile Das U-Boot"
  echo "########################"
  cd ${UBOOT_DIR}
  echo "COMPILER_OPT = ${COMPILER_OPT}"
  echo "make u-boot with config ${UBOOT_CONFIG}"
  make ${COMPILER_OPT} ${UBOOT_CONFIG}
  if [ "${OPENSBI_FROM}" != "" ]
  then
    make ${COMPILER_OPT} OPENSBI="../${OPENSBI_BIN}"
  fi
  cd ..
  # cree u-boot-sunxi-with-spl.bin
}

uboot_install()
{
  DATE=`date +%Y-%m-%d_%H:%M:%S`

  backup="${BACKUP_DIR}/u-boot_backup.$DATE"
  if [ ! -e "${BACKUP_DIR}" ]
  then
    mkdir "${BACKUP_DIR}"
  fi
  echo "do an 1MB backup of the current u-boot in ${backup}"

  # TODO backup the written size computer from file  and bs, round above if needed
  UBOOT_SIZE=`ls -l ${UBOOT_DIR}/${UBOOT_BIN} | awk '{print $5}'`
  echo "UBOOT_SIZE = ${UBOOT_SIZE}"
  TMP_SIZE=`expr ${UBOOT_SIZE} / ${UBOOT_BS}`
  MUL_SIZE=`expr ${TMP_SIZE} \* ${UBOOT_BS}`
  DIFF=`expr ${MUL_SIZE} - ${UBOOT_SIZE}`
  if [ ${DIFF} -lt 0 ]; then
    echo "round above"
    BACKUP_SIZE=`expr ${TMP_SIZE} + 1`
  else
    BACKUP_SIZE=${TMP_SIZE}
  fi
  echo "BACKUP_SIZE = ${BACKUP_SIZE} * ${UBOOT_BS} = `expr ${BACKUP_SIZE} \* ${UBOOT_BS}`"
  if [ ${UBOOT_BS} -eq 1024 ]
  then
    dd if="${DEVICE}" of=${backup} bs=${UBOOT_BS} skip=${UBOOT_SEEK} count=${BACKUP_SIZE}
  else
    dd if="${DEVICE}" of=${backup} bs=${UBOOT_BS} skip=${UBOOT_SEEK} count=${BACKUP_SIZE}
  fi
  #example:
  #dd if=u-boot/u-boot-sunxi-with-spl.bin of=/dev/mmcblk0 bs=1024 seek=128
  echo "dd if=${UBOOT_DIR}/${UBOOT_BIN} of="${DEVICE}" bs=${UBOOT_BS} seek=${UBOOT_SEEK}"
  dd if=${UBOOT_DIR}/${UBOOT_BIN} of="${DEVICE}" bs=${UBOOT_BS} seek=${UBOOT_SEEK}
}

goto_target_dir

# architecture dependent
if [ "${ARCH}" == "riscv" ]
then
  opensbi_fetch
elif [ "${RKBIN_SOURCE}" != "" ]
then
  rkbin_fetch
fi

# uboot itself
uboot_fetch
uboot_config

# rkbin need a first compilation of uboot on non-x86_64 platform to reuse mkimage
# https://github.com/edk2-porting/edk2-rk35xx/commit/17c74c1f17ce0e74de3658e769d432f15a057dcc

if [ "${RKBIN_DIR}" != "" ]
then
  cp -a  "${RKBIN_DIR}/${RKBIN_BIN}" "${UBOOT_DIR}/${RKBIN_DEST}"
fi

uboot_compile
uboot_install
goto_base_dir
