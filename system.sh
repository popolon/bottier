#!/usr/bin/env bash
TARGET_PLATFORM=$1
DEVICE=$2
source helpers/OPTIONS.sh ${TARGET_PLATFORM} ${DEVICE}


rootfs_fetch()
{
  bottier_fetch "${OS_URL}"  
}

rootfs_install()
{
  #pwd
  # builder/${TARGET_PLATFORM}
  echo bsdtar -xpf "${OS_FILE}" -C "${MNT}"
  sudo bsdtar -xpf "${OS_FILE}" -C "${MNT}"
}

# Get rootfs
rootfs_fetch

# Mount fs
mount_fs

# Install rootfs
rootfs_install

# Umount fs and sync
umount_fs

echo "DONE, you can reboot or unplug device safely"
