#!/usr/bin/env bash
TARGET_PLATFORM=$1
DEVICE=$2
source helpers/OPTIONS.sh ${TARGET_PLATFORM} ${DEVICE}

############################
## init partitions settings
############################
echo "partition table type: ${STORAGE_LABEL}"
echo "${#STORAGE_PARTS[@]} partitions:"
part=()

set_partition_elts()
{
i=$1
  if [ "${i}" == "" ]
  then
    echo "set_partition_elts(): no partition number given"
    exit -10
  fi
  echo "STORAGE_PARTS[${i}] = ${STORAGE_PARTS[${i}]}"
  part=()
  for elt in ${STORAGE_PARTS[${i}]}
  do
    part+=($elt)
  done
  echo "$i: name: ${part[0]} type: ${part[1]} format: ${part[2]} start: ${part[3]} end: ${part[4]}"
}

DEBUG=1
if [ $DEBUG -ne 0 ]
then
  echo "partition table type: ${STORAGE_LABEL}" 
  echo "${#STORAGE_PARTS[@]} partitions:"
  for((i=0;i<${#STORAGE_PARTS[@]};i++))
  do
    set_partition_elts ${i}
  done
fi


##############
## functions 
##############
backup()
{
  echo "TODO"
  echo "device: ${DEVICE}"
}

wipe()
{
  are_you_really_sure "wipe the entire ${DEVICE}, it contains currently `fdisk -l ${DEVICE}`"
  rc="$?"
  if [ ${rc} == 0 ]
  then
    echo "..wipefs -a ${DEVICE}"
    wipefs -a ${DEVICE}
  else
    echo "cancel all and exit"
    exit 0
  fi
}

backup_boot()
{
  echo "..backup boot (TOTEST)"
  dd 
}

backup_partition_table()
{
  echo "..backup parition table (TODO)"
}

backup()
{
  echo "not well tested"
  echo "device: ${DEVICE} ${image_file}"
  if [ ! -e "backups" ]
  then
    mkdir "backups"
  fi
  backup_boot
}

create_partition_table()
{
  echo "..create partition table"
  echo parted -s -a optimal -- "${DEVICE}" mklabel ${STORAGE_LABEL}
  parted -s -a optimal -- "${DEVICE}" mklabel ${STORAGE_LABEL}
  for((i=0;i<${#STORAGE_PARTS[@]};i++))
  do
    set_partition_elts ${i}
    echo "$i: name: ${part[0]} type: ${part[1]} format: ${part[2]} start: ${part[3]} end: ${part[4]}"
    name=${part[0]}; type=${part[1]}; format=${part[2]}; start=${part[3]}; end=${part[4]}
    echo parted -s -a optimal -- "${DEVICE}" mkpart ${type} ${format} ${start} ${end}
    parted -s -a optimal -- "${DEVICE}" mkpart ${type} ${format} ${start} ${end}
    echo mkfs.${format} -F -L ${name} "${DEVICE}${PART_IDENTITYFIER}p$((i+1))"
    mkfs.${format} -F -L ${name} "${DEVICE}${PART_IDENTITYFIER}p$((i+1))"
  done 
  echo partprobe "${DEVICE}"
}

if [ "${DEVICE}" != "" ]
then
#   backup
   wipe
fi

create_partition_table
sync
unloop

