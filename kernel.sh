#!/usr/bin/env bash
TARGET_PLATFORM=$1
DEVICE=$2
source helpers/OPTIONS.sh ${TARGET_PLATFORM} ${DEVICE}

## Small kernel changes for staging
kernel_add()
{
KER_FILE=$1
KER_ADD=$2
KER_SEP=$3
 if [ "${KER_SEP}" != "" ]
 then
   echo adding `echo "${KER_ADD}" | tr "${KER_SEP}" " "` to "${KER_FILE}"
   echo "${KER_ADD}" | tr "${KER_SEP}" " " >>"${KER_FILE}"
 else
   echo adding "${KER_ADD}" to "${KER_FILE}" 
   echo "${KER_ADD}" >>"${KER_FILE}"
 fi
}

## Get and patch kernel
kernel_fetch()
{
  echo "fetching kernel"
  if [ "${KERNEL_DOWN}" == "git" ]
  then
    bottier_fetch "${KERNEL_URL}" git ${KERNEL_BRANCH}
  else
    bottier_fetch "${KERNEL_URL}" untar
  fi


  # major patch, like from mainline-rcX to linux-next
  if [ "${KERNEL_PATCH}" != "" ]
  then
    echo "..fetching general patch ${PATCH_URL}"
    bottier_fetch "${PATCH_URL}"
    echo "..apply ${patch-${KERNEL_RELEASE}.xz}"
    cd ${KERNEL_DIR}
    apply_patch "`pwd`" "patch-${KERNEL_RELEASE}.xz"
    cd ..
  fi

  # minors patchs
  if [ "${PATCHS}" != "" ]
  then
    cd ${KERNEL_DIR}
    for PATCH in ${PATCHS}
    do
      if [ "${PATCH:0:4}" == "http" ]
      then
	BASENAME=`basename ${PATCH}`
	if [ ! -e ../${BASENAME} ]
        then
          echo "..fetching external patch ${PATCH}"
          wget -O ../${BASENAME} ${PATCH}
	fi
	echo "..apply patch ../${BASENAME}"
        apply_patch "`pwd`" "builder/${TARGET_PLATFORM}/${BASENAME}"
      else
        echo "..apply patch ${PATCH}"
        apply_patch "`pwd`" "${PATCH}"
      fi
    done
    cd ..
  fi

  # staging settings
  cd ${KERNEL_DIR}
  for((i=0;i<${#KERNEL_SRC_ADDITIONS[@]};i++))
  do
    ker_add=()
    for elt in ${KERNEL_SRC_ADDITIONS[${i}]}
    do
      ker_add+=($elt)
    done
    echo -e "..\e[1mkernel addition:\e[0m"
    kernel_add ${ker_add[0]} ${ker_add[1]} ${ker_add[2]}
  done
  cd ..
}

## kernel configuration
kernel_config()
{
  echo "kernel config"
  pwd
  cd ${KERNEL_DIR}
  if [ "$KERNEL_CONFIG_FROM" != "" ]
  then
    case "${KERNEL_CONFIG_FROM}" in
      'kerneldir')
        echo "..from kerneldir:  ${KERNEL_CONFIG}"
        echo cp -a ${KERNEL_CONFIG} .config
        cp -a ${KERNEL_CONFIG} .config
	;;
      'file')
        if [ "${KERNEL_CONFIG:0:1}" != "/" ]
        then # not absolute path
	  echo "..use ${BASE_DIR}/${KERNEL_CONFIG} oldconfig"
          filecat ${BASE_DIR}/${KERNEL_CONFIG} >.config
        else
	  echo "..use  ${KERNEL_CONFIG} oldconfig"
	  filecat ${KERNEL_CONFIG} >.config
        fi
        make ${COMPILER_OPT} oldconfig
        ;;
      'menuconfig')
	make ${COMPILER_OPT} menuconfig
	;;
      *)
        make ${COMPILER_OPT} menuconfig
	;;
    esac
  fi
  ####
  #### to help improve an existing TODO: if KERNEL_CONFIG_FROM == "file/kerneldir+menuconfig"
  #make ${COMPILER_OPT} menuconfig
  ###
  # exit 0
  ###
  cd ..
}

## Kernel compilation
kernel_compile()
{
  echo "kernel compile"
  cd ${KERNEL_DIR}
  make ${COMPILER_OPT}
  cd ..
}

## Kernel installation
kernel_install()
{
  echo "kernel_install"
  cd ${KERNEL_DIR}
  KERNEL_NAME=`cat include/config/kernel.release`
  echo "Kernel release: ${KERNEL_NAME}"
  ## install kernel, modules and dtbs
  echo "..make ${COMPILER_OPT} INSTALL_PATH=$MNT/boot install"
  sudo make ${COMPILER_OPT} INSTALL_PATH=$MNT/boot install
  echo "..make ${COMPILER_OPT} INSTALL_PATH=$MNT/boot dtbs_install"
  sudo make ${COMPILER_OPT} INSTALL_PATH=$MNT/boot dtbs_install
  echo "../..make ${COMPILER_OPT} INSTALL_MOD_PATH=$MNT modules_install"
  sudo make ${COMPILER_OPT} INSTALL_MOD_PATH=$MNT modules_install

  ## modify extlinux conf
  if [ -e "${MNT}/boot/extlinux/extlinux.conf" ]
  then
    DATE=`date +%Y-%m-%d_%H:%M:%S --reference="${MNT}/boot/extlinux/extlinux.conf"`
    sudo cp -a "${MNT}/boot/extlinux/extlinux.conf" "${MNT}/boot/extlinux/extlinux.conf.$DATE"
  else
    if [ ! -e "${MNT}/boot/extlinux" ]
    then
      sudo mkdir "${MNT}/boot/extlinux"
    fi
  fi
  sed "s/KERNEL_VERSION/${KERNEL_NAME}/" <${BASE_DIR}/conf/extlinux_template.conf >/tmp/extlinux.conf
  sudo cp -a /tmp/extlinux.conf "${MNT}/boot/extlinux/extlinux.conf"
  cd ..
}

## Fetch external modules
module_fetch()
{
MOD_URL=$1
MOD_DOWN=$2
  if [ "${MOD_DOWN}" == "git" ]
  then
    bottier_fetch "${MOD_URL}" git
  else
    bottier_fetch "${MOD_URL}" untar
  fi
}

## Build individual external module
module_build()
{
MOD_URL=$1
LUP=${MOD_URL##*/}
MOD_DIR=${LUP//.git}
  cd ${MOD_DIR}
  make ${COMPILER_OPT}  KSRC=../${KERNEL_DIR} modules
  cd ..
}

## Install individual external module
module_install()
{
MOD_URL=$1
LUP=${MOD_URL##*/}
MOD_DIR=${LUP//.git}
MOD_NAME=$2
MODULE_PATH=$3
  cd ${MOD_DIR}
  echo "module_install ${MOD_NAME}"
  if [ ! -e "${MNT}/lib/modules/${KERNEL_NAME}/${MODULE_PATH}" ]
  then
    sudo mkdir -p "${MNT}/lib/modules/${KERNEL_NAME}/${MODULE_PATH}"
  fi
  sudo cp -a ${MOD_NAME}.ko ${MNT}/lib/modules/${KERNEL_NAME}/${MODULE_PATH}/
  #make ${MOD_DIR} INSTALL_MOD_PATH=${MNT} install
  if [ ! -e "${MNT}/etc/modules-load.d/${MOD_NAME}.conf" ]
  then
    if [ ! -e "${MNT}/etc/modules-load.d" ]
    then
      echo "${MNT}/etc/modules-load.d doesn't exist, create it"
      sudo mkdir -p "${MNT}/etc/modules-load.d"
    fi
    echo "${MOD_NAME}" >/tmp/${MOD_NAME}.conf
    sudo cp /tmp/${MOD_NAME}.conf "${MNT}/etc/modules-load.d/"
  fi
  cd ..
}

## launch external modules compilation
modules_compile()
{
  for((i=0;i<${#KERNEL_MODULES[@]};i++))
  do
    mod_info=()
    for elt in ${KERNEL_MODULES[${i}]}
    do
      mod_info+=($elt)
    done
    echo "module_fetch ${mod_info[0]} ${mod_info[1]}"
    module_fetch ${mod_info[0]} ${mod_info[1]}
    module_build ${mod_info[0]} ${mod_info[1]}
  done
}

## launche external modules installation
modules_install()
{
  for((i=0;i<${#KERNEL_MODULES[@]};i++))
  do
    mod_info=()
    for elt in ${KERNEL_MODULES[${i}]}
    do
      mod_info+=($elt)
    done
    module_install ${mod_info[0]} ${mod_info[2]} ${mod_info[3]}
  done
  sudo depmod -a -b "${MNT}" "${KERNEL_NAME}"
}

## Main
goto_target_dir
# Get kernel
kernel_fetch

# Configure and compile kernel
kernel_config
kernel_compile

# Compile external modules if there as
modules_compile

if [ "${DEVICE}" != "" ]
then
  echo "DEVICE=${DEVICE}, install the kernel on it"
  # mount fs
  mount_fs

  # Install kernel itself and it's DTB
  kernel_install

  # Install kernel modules
  modules_install

  # Umount fs and sync
  umount_fs
fi

goto_base_dir


